FROM ubuntu:latest
RUN apt update && apt install -y iputils-ping
CMD ["sh", "-c", "ping 10.0.2.2 -c 4 ; ping 10.0.2.3 -c 4; ping 10.0.2.4 -c 4 ; ping 10.0.2.5 -c 4"]
